/*
Name:		Taibot.ino
Created:	12/13/2016 10:27:53 AM
Author:	Nahuel Taibo  savagemakers.com
*/

#include "Robot.h"

using namespace Taibot;

Robot robot;

void setup()
{
	// Configure the Serial to get the logging output
	Serial.begin(115200);

	Serial.println("Taibot Started.");
}

void loop()
{
	robot.Update();
}